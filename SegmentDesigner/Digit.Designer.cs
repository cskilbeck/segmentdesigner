﻿namespace SegmentDesigner
{
    partial class Digit
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelA = new System.Windows.Forms.Panel();
            this.panelF = new System.Windows.Forms.Panel();
            this.panelB = new System.Windows.Forms.Panel();
            this.panelG = new System.Windows.Forms.Panel();
            this.panelE = new System.Windows.Forms.Panel();
            this.panelC = new System.Windows.Forms.Panel();
            this.panelD = new System.Windows.Forms.Panel();
            this.panelP = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // panelA
            // 
            this.panelA.BackColor = System.Drawing.Color.DarkGray;
            this.panelA.Enabled = false;
            this.panelA.Location = new System.Drawing.Point(4, 0);
            this.panelA.Margin = new System.Windows.Forms.Padding(0);
            this.panelA.Name = "panelA";
            this.panelA.Size = new System.Drawing.Size(8, 4);
            this.panelA.TabIndex = 0;
            // 
            // panelF
            // 
            this.panelF.BackColor = System.Drawing.Color.Red;
            this.panelF.Enabled = false;
            this.panelF.Location = new System.Drawing.Point(0, 4);
            this.panelF.Margin = new System.Windows.Forms.Padding(0);
            this.panelF.Name = "panelF";
            this.panelF.Size = new System.Drawing.Size(4, 8);
            this.panelF.TabIndex = 1;
            // 
            // panelB
            // 
            this.panelB.BackColor = System.Drawing.Color.DimGray;
            this.panelB.Enabled = false;
            this.panelB.Location = new System.Drawing.Point(12, 4);
            this.panelB.Margin = new System.Windows.Forms.Padding(0);
            this.panelB.Name = "panelB";
            this.panelB.Size = new System.Drawing.Size(4, 8);
            this.panelB.TabIndex = 2;
            // 
            // panelG
            // 
            this.panelG.BackColor = System.Drawing.Color.Red;
            this.panelG.Enabled = false;
            this.panelG.Location = new System.Drawing.Point(4, 12);
            this.panelG.Margin = new System.Windows.Forms.Padding(0);
            this.panelG.Name = "panelG";
            this.panelG.Size = new System.Drawing.Size(8, 4);
            this.panelG.TabIndex = 1;
            // 
            // panelE
            // 
            this.panelE.BackColor = System.Drawing.Color.Red;
            this.panelE.Enabled = false;
            this.panelE.Location = new System.Drawing.Point(0, 16);
            this.panelE.Margin = new System.Windows.Forms.Padding(0);
            this.panelE.Name = "panelE";
            this.panelE.Size = new System.Drawing.Size(4, 8);
            this.panelE.TabIndex = 3;
            // 
            // panelC
            // 
            this.panelC.BackColor = System.Drawing.Color.Red;
            this.panelC.Enabled = false;
            this.panelC.Location = new System.Drawing.Point(12, 16);
            this.panelC.Margin = new System.Windows.Forms.Padding(0);
            this.panelC.Name = "panelC";
            this.panelC.Size = new System.Drawing.Size(4, 8);
            this.panelC.TabIndex = 4;
            // 
            // panelD
            // 
            this.panelD.BackColor = System.Drawing.Color.Red;
            this.panelD.Enabled = false;
            this.panelD.Location = new System.Drawing.Point(4, 24);
            this.panelD.Margin = new System.Windows.Forms.Padding(0);
            this.panelD.Name = "panelD";
            this.panelD.Size = new System.Drawing.Size(8, 4);
            this.panelD.TabIndex = 5;
            // 
            // panelP
            // 
            this.panelP.BackColor = System.Drawing.Color.Red;
            this.panelP.Enabled = false;
            this.panelP.Location = new System.Drawing.Point(18, 24);
            this.panelP.Margin = new System.Windows.Forms.Padding(0);
            this.panelP.Name = "panelP";
            this.panelP.Size = new System.Drawing.Size(4, 4);
            this.panelP.TabIndex = 6;
            // 
            // Digit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.Controls.Add(this.panelP);
            this.Controls.Add(this.panelD);
            this.Controls.Add(this.panelC);
            this.Controls.Add(this.panelE);
            this.Controls.Add(this.panelG);
            this.Controls.Add(this.panelB);
            this.Controls.Add(this.panelF);
            this.Controls.Add(this.panelA);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MaximumSize = new System.Drawing.Size(22, 28);
            this.MinimumSize = new System.Drawing.Size(22, 28);
            this.Name = "Digit";
            this.Size = new System.Drawing.Size(22, 28);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelA;
        private System.Windows.Forms.Panel panelF;
        private System.Windows.Forms.Panel panelG;
        private System.Windows.Forms.Panel panelB;
        private System.Windows.Forms.Panel panelE;
        private System.Windows.Forms.Panel panelC;
        private System.Windows.Forms.Panel panelD;
        private System.Windows.Forms.Panel panelP;
    }
}
