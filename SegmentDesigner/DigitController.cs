﻿using System;
using System.Windows.Forms;

namespace SegmentDesigner
{
    public partial class DigitController : UserControl
    {
        public event EventHandler Activated;
        public event EventHandler Loader;
        public int bitmap;

        public DigitController()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(Activated != null)
            {
                Activated(this, e);
            }
        }

        public int Get()
        {
            return bitmap;
        }

        public void Set(int n)
        {
            bitmap = n;
            digit1.Set(n);
            textBox27.Text = Convert.ToString(n, 2).PadLeft(8, '0');
        }

        public void SetButtonText()
        {
            button2.Text = Tag as string;
        }

        public void StringSet(string s)
        {
            int p = 1;
            int t = 0;
            for(int i=7; i>=0; --i)
            {
                if (s[i] == '1')
                {
                    t += p;
                }
                p <<= 1;
            }
            Set(t);
        }

        private void digit1_Click(object sender, EventArgs e)
        {
            if(Loader != null)
            {
                Loader(this, e);
            }
        }
    }
}
