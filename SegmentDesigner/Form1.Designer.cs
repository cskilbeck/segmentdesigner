﻿namespace SegmentDesigner
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelA = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panelB = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panelF = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panelE = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panelG = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panelD = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panelC = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panelP = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.digitController19 = new SegmentDesigner.DigitController();
            this.digitController18 = new SegmentDesigner.DigitController();
            this.digitController17 = new SegmentDesigner.DigitController();
            this.digitController16 = new SegmentDesigner.DigitController();
            this.digitController15 = new SegmentDesigner.DigitController();
            this.digitController14 = new SegmentDesigner.DigitController();
            this.digitController13 = new SegmentDesigner.DigitController();
            this.digitController12 = new SegmentDesigner.DigitController();
            this.digitController11 = new SegmentDesigner.DigitController();
            this.digitController10 = new SegmentDesigner.DigitController();
            this.digitController09 = new SegmentDesigner.DigitController();
            this.digitController08 = new SegmentDesigner.DigitController();
            this.digitController07 = new SegmentDesigner.DigitController();
            this.digitController06 = new SegmentDesigner.DigitController();
            this.digitController05 = new SegmentDesigner.DigitController();
            this.digitController04 = new SegmentDesigner.DigitController();
            this.digitController03 = new SegmentDesigner.DigitController();
            this.digitController02 = new SegmentDesigner.DigitController();
            this.digitController01 = new SegmentDesigner.DigitController();
            this.digitController00 = new SegmentDesigner.DigitController();
            this.panelA.SuspendLayout();
            this.panelB.SuspendLayout();
            this.panelF.SuspendLayout();
            this.panelE.SuspendLayout();
            this.panelG.SuspendLayout();
            this.panelD.SuspendLayout();
            this.panelC.SuspendLayout();
            this.panelP.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelA
            // 
            this.panelA.BackColor = System.Drawing.Color.Silver;
            this.panelA.Controls.Add(this.label1);
            this.panelA.Location = new System.Drawing.Point(38, 24);
            this.panelA.Margin = new System.Windows.Forms.Padding(0);
            this.panelA.Name = "panelA";
            this.panelA.Size = new System.Drawing.Size(104, 28);
            this.panelA.TabIndex = 0;
            this.panelA.Tag = "0";
            this.panelA.Click += new System.EventHandler(this.panelA_Click);
            this.panelA.DoubleClick += new System.EventHandler(this.panelA_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Enabled = false;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "A";
            // 
            // panelB
            // 
            this.panelB.BackColor = System.Drawing.Color.Silver;
            this.panelB.Controls.Add(this.label2);
            this.panelB.Location = new System.Drawing.Point(124, 52);
            this.panelB.Margin = new System.Windows.Forms.Padding(0);
            this.panelB.Name = "panelB";
            this.panelB.Size = new System.Drawing.Size(28, 112);
            this.panelB.TabIndex = 1;
            this.panelB.Tag = "1";
            this.panelB.Click += new System.EventHandler(this.panelA_Click);
            this.panelB.DoubleClick += new System.EventHandler(this.panelA_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Enabled = false;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "B";
            // 
            // panelF
            // 
            this.panelF.BackColor = System.Drawing.Color.Silver;
            this.panelF.Controls.Add(this.label6);
            this.panelF.Location = new System.Drawing.Point(28, 52);
            this.panelF.Margin = new System.Windows.Forms.Padding(0);
            this.panelF.Name = "panelF";
            this.panelF.Size = new System.Drawing.Size(28, 112);
            this.panelF.TabIndex = 2;
            this.panelF.Tag = "5";
            this.panelF.Click += new System.EventHandler(this.panelA_Click);
            this.panelF.DoubleClick += new System.EventHandler(this.panelA_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Enabled = false;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "F";
            // 
            // panelE
            // 
            this.panelE.BackColor = System.Drawing.Color.Silver;
            this.panelE.Controls.Add(this.label5);
            this.panelE.Location = new System.Drawing.Point(28, 192);
            this.panelE.Margin = new System.Windows.Forms.Padding(0);
            this.panelE.Name = "panelE";
            this.panelE.Size = new System.Drawing.Size(28, 112);
            this.panelE.TabIndex = 3;
            this.panelE.Tag = "4";
            this.panelE.Click += new System.EventHandler(this.panelA_Click);
            this.panelE.DoubleClick += new System.EventHandler(this.panelA_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Enabled = false;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "E";
            // 
            // panel5
            // 
            this.panel5.Location = new System.Drawing.Point(219, 6);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(27, 168);
            this.panel5.TabIndex = 4;
            // 
            // panelG
            // 
            this.panelG.BackColor = System.Drawing.Color.Silver;
            this.panelG.Controls.Add(this.label7);
            this.panelG.Controls.Add(this.panel5);
            this.panelG.Location = new System.Drawing.Point(38, 164);
            this.panelG.Margin = new System.Windows.Forms.Padding(0);
            this.panelG.Name = "panelG";
            this.panelG.Size = new System.Drawing.Size(104, 28);
            this.panelG.TabIndex = 1;
            this.panelG.Tag = "6";
            this.panelG.Click += new System.EventHandler(this.panelA_Click);
            this.panelG.DoubleClick += new System.EventHandler(this.panelA_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Enabled = false;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(43, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "G";
            // 
            // panelD
            // 
            this.panelD.BackColor = System.Drawing.Color.Silver;
            this.panelD.Controls.Add(this.label4);
            this.panelD.Location = new System.Drawing.Point(38, 304);
            this.panelD.Margin = new System.Windows.Forms.Padding(0);
            this.panelD.Name = "panelD";
            this.panelD.Size = new System.Drawing.Size(104, 28);
            this.panelD.TabIndex = 2;
            this.panelD.Tag = "3";
            this.panelD.Click += new System.EventHandler(this.panelA_Click);
            this.panelD.DoubleClick += new System.EventHandler(this.panelA_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Enabled = false;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(42, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "D";
            // 
            // panelC
            // 
            this.panelC.BackColor = System.Drawing.Color.Silver;
            this.panelC.Controls.Add(this.label3);
            this.panelC.Location = new System.Drawing.Point(124, 192);
            this.panelC.Margin = new System.Windows.Forms.Padding(0);
            this.panelC.Name = "panelC";
            this.panelC.Size = new System.Drawing.Size(28, 112);
            this.panelC.TabIndex = 2;
            this.panelC.Tag = "2";
            this.panelC.Click += new System.EventHandler(this.panelA_Click);
            this.panelC.DoubleClick += new System.EventHandler(this.panelA_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Enabled = false;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "C";
            // 
            // panelP
            // 
            this.panelP.BackColor = System.Drawing.Color.Silver;
            this.panelP.Controls.Add(this.label8);
            this.panelP.Location = new System.Drawing.Point(164, 304);
            this.panelP.Margin = new System.Windows.Forms.Padding(0);
            this.panelP.Name = "panelP";
            this.panelP.Size = new System.Drawing.Size(28, 28);
            this.panelP.TabIndex = 3;
            this.panelP.Tag = "7";
            this.panelP.Click += new System.EventHandler(this.panelA_Click);
            this.panelP.DoubleClick += new System.EventHandler(this.panelA_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Enabled = false;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "P";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(319, 188);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(53, 17);
            this.checkBox1.TabIndex = 29;
            this.checkBox1.Text = "Invert";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(297, 211);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 30;
            this.button1.Text = "Copy";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Courier New", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(210, 24);
            this.textBox1.MaxLength = 8;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(162, 35);
            this.textBox1.TabIndex = 31;
            this.textBox1.Text = "PGFEDCBA";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Font = new System.Drawing.Font("Courier New", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(210, 65);
            this.textBox3.MaxLength = 8;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(162, 35);
            this.textBox3.TabIndex = 33;
            this.textBox3.Text = "76543210";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Font = new System.Drawing.Font("Courier New", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(210, 106);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(162, 35);
            this.textBox4.TabIndex = 34;
            this.textBox4.Text = "00000000";
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox6
            // 
            this.textBox6.Enabled = false;
            this.textBox6.Font = new System.Drawing.Font("Courier New", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(210, 147);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(162, 35);
            this.textBox6.TabIndex = 36;
            this.textBox6.Text = "00";
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // digitController19
            // 
            this.digitController19.Location = new System.Drawing.Point(532, 321);
            this.digitController19.Margin = new System.Windows.Forms.Padding(0);
            this.digitController19.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController19.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController19.Name = "digitController19";
            this.digitController19.Size = new System.Drawing.Size(120, 28);
            this.digitController19.TabIndex = 156;
            this.digitController19.Tag = "19";
            // 
            // digitController18
            // 
            this.digitController18.Location = new System.Drawing.Point(532, 288);
            this.digitController18.Margin = new System.Windows.Forms.Padding(0);
            this.digitController18.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController18.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController18.Name = "digitController18";
            this.digitController18.Size = new System.Drawing.Size(120, 28);
            this.digitController18.TabIndex = 155;
            this.digitController18.Tag = "18";
            // 
            // digitController17
            // 
            this.digitController17.Location = new System.Drawing.Point(532, 255);
            this.digitController17.Margin = new System.Windows.Forms.Padding(0);
            this.digitController17.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController17.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController17.Name = "digitController17";
            this.digitController17.Size = new System.Drawing.Size(120, 28);
            this.digitController17.TabIndex = 154;
            this.digitController17.Tag = "17";
            // 
            // digitController16
            // 
            this.digitController16.Location = new System.Drawing.Point(532, 222);
            this.digitController16.Margin = new System.Windows.Forms.Padding(0);
            this.digitController16.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController16.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController16.Name = "digitController16";
            this.digitController16.Size = new System.Drawing.Size(120, 28);
            this.digitController16.TabIndex = 153;
            this.digitController16.Tag = "16";
            // 
            // digitController15
            // 
            this.digitController15.Location = new System.Drawing.Point(532, 189);
            this.digitController15.Margin = new System.Windows.Forms.Padding(0);
            this.digitController15.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController15.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController15.Name = "digitController15";
            this.digitController15.Size = new System.Drawing.Size(120, 28);
            this.digitController15.TabIndex = 152;
            this.digitController15.Tag = "15";
            // 
            // digitController14
            // 
            this.digitController14.Location = new System.Drawing.Point(532, 156);
            this.digitController14.Margin = new System.Windows.Forms.Padding(0);
            this.digitController14.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController14.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController14.Name = "digitController14";
            this.digitController14.Size = new System.Drawing.Size(120, 28);
            this.digitController14.TabIndex = 151;
            this.digitController14.Tag = "14";
            // 
            // digitController13
            // 
            this.digitController13.Location = new System.Drawing.Point(532, 123);
            this.digitController13.Margin = new System.Windows.Forms.Padding(0);
            this.digitController13.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController13.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController13.Name = "digitController13";
            this.digitController13.Size = new System.Drawing.Size(120, 28);
            this.digitController13.TabIndex = 150;
            this.digitController13.Tag = "13";
            // 
            // digitController12
            // 
            this.digitController12.Location = new System.Drawing.Point(532, 90);
            this.digitController12.Margin = new System.Windows.Forms.Padding(0);
            this.digitController12.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController12.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController12.Name = "digitController12";
            this.digitController12.Size = new System.Drawing.Size(120, 28);
            this.digitController12.TabIndex = 149;
            this.digitController12.Tag = "12";
            // 
            // digitController11
            // 
            this.digitController11.Location = new System.Drawing.Point(532, 57);
            this.digitController11.Margin = new System.Windows.Forms.Padding(0);
            this.digitController11.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController11.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController11.Name = "digitController11";
            this.digitController11.Size = new System.Drawing.Size(120, 28);
            this.digitController11.TabIndex = 148;
            this.digitController11.Tag = "11";
            // 
            // digitController10
            // 
            this.digitController10.Location = new System.Drawing.Point(532, 24);
            this.digitController10.Margin = new System.Windows.Forms.Padding(0);
            this.digitController10.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController10.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController10.Name = "digitController10";
            this.digitController10.Size = new System.Drawing.Size(120, 28);
            this.digitController10.TabIndex = 147;
            this.digitController10.Tag = "10";
            // 
            // digitController09
            // 
            this.digitController09.Location = new System.Drawing.Point(389, 321);
            this.digitController09.Margin = new System.Windows.Forms.Padding(0);
            this.digitController09.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController09.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController09.Name = "digitController09";
            this.digitController09.Size = new System.Drawing.Size(120, 28);
            this.digitController09.TabIndex = 146;
            this.digitController09.Tag = "9";
            // 
            // digitController08
            // 
            this.digitController08.Location = new System.Drawing.Point(389, 288);
            this.digitController08.Margin = new System.Windows.Forms.Padding(0);
            this.digitController08.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController08.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController08.Name = "digitController08";
            this.digitController08.Size = new System.Drawing.Size(120, 28);
            this.digitController08.TabIndex = 145;
            this.digitController08.Tag = "8";
            // 
            // digitController07
            // 
            this.digitController07.Location = new System.Drawing.Point(389, 255);
            this.digitController07.Margin = new System.Windows.Forms.Padding(0);
            this.digitController07.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController07.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController07.Name = "digitController07";
            this.digitController07.Size = new System.Drawing.Size(120, 28);
            this.digitController07.TabIndex = 144;
            this.digitController07.Tag = "7";
            // 
            // digitController06
            // 
            this.digitController06.Location = new System.Drawing.Point(389, 222);
            this.digitController06.Margin = new System.Windows.Forms.Padding(0);
            this.digitController06.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController06.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController06.Name = "digitController06";
            this.digitController06.Size = new System.Drawing.Size(120, 28);
            this.digitController06.TabIndex = 143;
            this.digitController06.Tag = "6";
            // 
            // digitController05
            // 
            this.digitController05.Location = new System.Drawing.Point(389, 189);
            this.digitController05.Margin = new System.Windows.Forms.Padding(0);
            this.digitController05.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController05.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController05.Name = "digitController05";
            this.digitController05.Size = new System.Drawing.Size(120, 28);
            this.digitController05.TabIndex = 142;
            this.digitController05.Tag = "5";
            // 
            // digitController04
            // 
            this.digitController04.Location = new System.Drawing.Point(389, 156);
            this.digitController04.Margin = new System.Windows.Forms.Padding(0);
            this.digitController04.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController04.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController04.Name = "digitController04";
            this.digitController04.Size = new System.Drawing.Size(120, 28);
            this.digitController04.TabIndex = 141;
            this.digitController04.Tag = "4";
            // 
            // digitController03
            // 
            this.digitController03.Location = new System.Drawing.Point(389, 123);
            this.digitController03.Margin = new System.Windows.Forms.Padding(0);
            this.digitController03.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController03.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController03.Name = "digitController03";
            this.digitController03.Size = new System.Drawing.Size(120, 28);
            this.digitController03.TabIndex = 140;
            this.digitController03.Tag = "3";
            // 
            // digitController02
            // 
            this.digitController02.Location = new System.Drawing.Point(389, 90);
            this.digitController02.Margin = new System.Windows.Forms.Padding(0);
            this.digitController02.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController02.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController02.Name = "digitController02";
            this.digitController02.Size = new System.Drawing.Size(120, 28);
            this.digitController02.TabIndex = 139;
            this.digitController02.Tag = "2";
            // 
            // digitController01
            // 
            this.digitController01.Location = new System.Drawing.Point(389, 57);
            this.digitController01.Margin = new System.Windows.Forms.Padding(0);
            this.digitController01.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController01.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController01.Name = "digitController01";
            this.digitController01.Size = new System.Drawing.Size(120, 28);
            this.digitController01.TabIndex = 138;
            this.digitController01.Tag = "1";
            // 
            // digitController00
            // 
            this.digitController00.Location = new System.Drawing.Point(389, 24);
            this.digitController00.Margin = new System.Windows.Forms.Padding(0);
            this.digitController00.MaximumSize = new System.Drawing.Size(120, 28);
            this.digitController00.MinimumSize = new System.Drawing.Size(120, 28);
            this.digitController00.Name = "digitController00";
            this.digitController00.Size = new System.Drawing.Size(120, 28);
            this.digitController00.TabIndex = 137;
            this.digitController00.Tag = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 369);
            this.Controls.Add(this.digitController19);
            this.Controls.Add(this.digitController18);
            this.Controls.Add(this.digitController17);
            this.Controls.Add(this.digitController16);
            this.Controls.Add(this.digitController15);
            this.Controls.Add(this.digitController14);
            this.Controls.Add(this.digitController13);
            this.Controls.Add(this.digitController12);
            this.Controls.Add(this.digitController11);
            this.Controls.Add(this.digitController10);
            this.Controls.Add(this.digitController09);
            this.Controls.Add(this.digitController08);
            this.Controls.Add(this.digitController07);
            this.Controls.Add(this.digitController06);
            this.Controls.Add(this.digitController05);
            this.Controls.Add(this.digitController04);
            this.Controls.Add(this.digitController03);
            this.Controls.Add(this.digitController02);
            this.Controls.Add(this.digitController01);
            this.Controls.Add(this.digitController00);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.panelP);
            this.Controls.Add(this.panelC);
            this.Controls.Add(this.panelD);
            this.Controls.Add(this.panelG);
            this.Controls.Add(this.panelE);
            this.Controls.Add(this.panelF);
            this.Controls.Add(this.panelB);
            this.Controls.Add(this.panelA);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Segment Designer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelA.ResumeLayout(false);
            this.panelA.PerformLayout();
            this.panelB.ResumeLayout(false);
            this.panelB.PerformLayout();
            this.panelF.ResumeLayout(false);
            this.panelF.PerformLayout();
            this.panelE.ResumeLayout(false);
            this.panelE.PerformLayout();
            this.panelG.ResumeLayout(false);
            this.panelG.PerformLayout();
            this.panelD.ResumeLayout(false);
            this.panelD.PerformLayout();
            this.panelC.ResumeLayout(false);
            this.panelC.PerformLayout();
            this.panelP.ResumeLayout(false);
            this.panelP.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelA;
        private System.Windows.Forms.Panel panelB;
        private System.Windows.Forms.Panel panelF;
        private System.Windows.Forms.Panel panelE;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panelG;
        private System.Windows.Forms.Panel panelD;
        private System.Windows.Forms.Panel panelC;
        private System.Windows.Forms.Panel panelP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox6;
        private DigitController digitController00;
        private DigitController digitController01;
        private DigitController digitController03;
        private DigitController digitController02;
        private DigitController digitController07;
        private DigitController digitController06;
        private DigitController digitController05;
        private DigitController digitController04;
        private DigitController digitController09;
        private DigitController digitController08;
        private DigitController digitController19;
        private DigitController digitController18;
        private DigitController digitController17;
        private DigitController digitController16;
        private DigitController digitController15;
        private DigitController digitController14;
        private DigitController digitController13;
        private DigitController digitController12;
        private DigitController digitController11;
        private DigitController digitController10;
    }
}

