﻿namespace SegmentDesigner
{
    partial class DigitController
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.digit1 = new SegmentDesigner.Digit();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 23);
            this.button2.TabIndex = 119;
            this.button2.Text = "00";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox27
            // 
            this.textBox27.Enabled = false;
            this.textBox27.Location = new System.Drawing.Point(36, 4);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(56, 20);
            this.textBox27.TabIndex = 118;
            this.textBox27.Text = "00000000";
            // 
            // digit1
            // 
            this.digit1.BackColor = System.Drawing.Color.Gray;
            this.digit1.Location = new System.Drawing.Point(97, 0);
            this.digit1.Margin = new System.Windows.Forms.Padding(0);
            this.digit1.MaximumSize = new System.Drawing.Size(22, 28);
            this.digit1.MinimumSize = new System.Drawing.Size(22, 28);
            this.digit1.Name = "digit1";
            this.digit1.Size = new System.Drawing.Size(22, 28);
            this.digit1.TabIndex = 120;
            this.digit1.Click += new System.EventHandler(this.digit1_Click);
            // 
            // DigitController
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.digit1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox27);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MaximumSize = new System.Drawing.Size(120, 28);
            this.MinimumSize = new System.Drawing.Size(120, 28);
            this.Name = "DigitController";
            this.Size = new System.Drawing.Size(120, 28);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Digit digit1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox27;
    }
}
