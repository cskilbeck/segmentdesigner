﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SegmentDesigner
{
    public partial class Form1 : Form
    {
        List<DigitController> digits = new List<DigitController>();
        List<Panel> panels = new List<Panel>();
        bool[] seg = new bool[8] { false, false, false, false, false, false, false, false };
        int[] pin = new int[8] { 0, 1, 2, 3, 4, 5, 6, 7 };
        int bitmap = 0;
        int pinmask = 0;

        string[] init = new string[20] {
            "00111111", "00000110", "01011011", "01001111", "01100110",
            "01101101", "01111101", "00000111", "01111111", "01101111",
            "01110111", "01111100", "00111001", "01011110", "01111001",
            "01110001", "01000000", "01110110", "01101110", "00000000"
        };

        private void UpdatePins()
        {
            int i = 0;
            foreach (char c in "ABCDEFGP")
            {
                int pinindex = textBox1.Text.IndexOf(c);
                if (pinindex >= 0)
                {
                    pin[i++] = pinindex;
                }
            }
            UpdateBitmap();
        }

        private int Translate(int n)
        {
            int b = 0;
            for (int j = 0; j < 8; ++j)
            {
                b += ((n & (1<<j)) != 0) ? (0x80 >> pin[j]) : 0;
            }
            return checkBox1.Checked ? (~b & 0xff) : b;
        }

        private void UpdateBitmap()
        {
            bitmap = 0;
            pinmask = 0;
            for (int j = 0; j < 8; ++j)
            {
                pinmask += seg[j] ? (1 << j) : 0;
                bitmap += seg[j] ? (0x80 >> pin[j]) : 0;
            }
            if(checkBox1.Checked)
            {
                bitmap = ~bitmap;
            }
            bitmap &= 0xff;
            textBox6.Text = Convert.ToString(bitmap, 16).PadLeft(2, '0');
            textBox4.Text = Convert.ToString(bitmap, 2).PadLeft(8, '0');
        }

        void UpdateSegments()
        {
            for(int i=0; i<8; ++i)
            {
                panels[i].BackColor = seg[i] ? Color.Red : Color.Silver;
            }
        }

        private void Toggle(Panel p)
        {
            UpdatePins();
            int i = int.Parse((string)p.Tag);
            seg[i] = !seg[i];
            p.BackColor = seg[i] ? Color.Red : Color.Silver;
            UpdateBitmap();
        }

        public Form1()
        {
            InitializeComponent();
            panels.Add(panelA);
            panels.Add(panelB);
            panels.Add(panelC);
            panels.Add(panelD);
            panels.Add(panelE);
            panels.Add(panelF);
            panels.Add(panelG);
            panels.Add(panelP);
            digits.Add(digitController00);
            digits.Add(digitController01);
            digits.Add(digitController02);
            digits.Add(digitController03);
            digits.Add(digitController04);
            digits.Add(digitController05);
            digits.Add(digitController06);
            digits.Add(digitController07);
            digits.Add(digitController08);
            digits.Add(digitController09);
            digits.Add(digitController10);
            digits.Add(digitController11);
            digits.Add(digitController12);
            digits.Add(digitController13);
            digits.Add(digitController14);
            digits.Add(digitController15);
            digits.Add(digitController16);
            digits.Add(digitController17);
            digits.Add(digitController18);
            digits.Add(digitController19);
            int i=0;
            foreach(DigitController d in digits)
            {
                d.SetButtonText();
                d.Activated += digitController00_Activated;
                d.Loader += d_Loader;
                d.StringSet(init[i++]);
            }
        }

        void d_Loader(object sender, EventArgs e)
        {
            int n = (sender as DigitController).Get();
            for(int i=0; i<8; ++i)
            {
                seg[i] = ((n >> i) & 1) != 0;
            }
            UpdateBitmap();
            UpdateSegments();
        }

        void digitController00_Activated(object sender, EventArgs e)
        {
            (sender as DigitController).Set(pinmask);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UpdatePins();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            UpdatePins();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBitmap();
        }

        private void panelA_Click(object sender, EventArgs e)
        {
            Toggle(sender as Panel);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StringBuilder s = new StringBuilder();
            foreach(DigitController d in digits)
            {
                int n = Translate(d.Get());
                s.Append(String.Format("{0}\n", Convert.ToString(n, 2).PadLeft(8, '0')));
            }
            Clipboard.SetText(s.ToString());
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
            e.Handled = e.KeyChar >= 32 && e.KeyChar < 98 && ("ABCDEFGP".IndexOf(e.KeyChar) < 0 || textBox1.Text.IndexOf(e.KeyChar) >= 0);
        }
    }
}
