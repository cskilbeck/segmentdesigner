﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace SegmentDesigner
{
    public partial class Digit : UserControl
    {
        private List<Panel> panels = new List<Panel>();

        public Digit()
        {
            InitializeComponent();
            panels.Add(panelA);
            panels.Add(panelB);
            panels.Add(panelC);
            panels.Add(panelD);
            panels.Add(panelE);
            panels.Add(panelF);
            panels.Add(panelG);
            panels.Add(panelP);
        }

        public void Set(int mask)
        {
            for(int i=0; i<8; ++i)
            {
                if((mask & (1 << i)) != 0)
                {
                    panels[i].BackColor = Color.FromArgb(255, 255, 32);
                }
                else
                {
                    panels[i].BackColor = Color.DimGray;
                }
            }
        }
    }
}
